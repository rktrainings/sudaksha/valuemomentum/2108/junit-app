package com.guruofjava.junitapptest;

import com.guruofjava.junitapp.Employee;
import java.util.Objects;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmployeeTest {

    @BeforeAll
    public static void beforeAll() {
        System.out.println("EmployeeTest BeforeAll");
    }

    @AfterAll
    public static void afterAll() {
        System.out.println("EmployeeTest AfterAll");
    }
    
    @BeforeEach
    public  void beforeEach(){
        System.out.println("@BeforeEach EmployeeTest");
    }
    
    
    @AfterEach
    public  void afterEach(){
        System.out.println("@AfterEach EmployeeTest");
    }
    
    @Test
    public void testDefaultEmployeeInstances(){
        Employee e1 = new Employee();
        Employee e2 = new Employee();
        Employee e3 = new Employee(1234, "James", "Architecture", 85000.0);
        
        assertTrue(e1.equals(e3), "Both new Employee instances are not same");
    }
    
    @Test
    public void testEmployeeSetName(){
        Employee e = new Employee();
        //e.setName("john");
        
        //Assumptions.assumeTrue(e.getName() != null);
        Assumptions.assumeTrue(Objects.nonNull(e.getName()), "Employee getName is null");
        
        assertTrue(e.getName().equals("John"));
    }

    @Test
    @DisplayName("Testing no parameter constructor of Employee")
    @Tag("fast")
    //@Disabled
    public void testBlankEmployee() {
        Employee e = new Employee();

        assertEquals(null, e.getEmployeeId());
        assertEquals(null, e.getName());
        assertEquals(null, e.getDepartment());
        assertEquals(null, e.getSalary());
        System.out.println("@Test testBlankEmployee() " + System.identityHashCode(this));
    }

    @Test
    @FastTag
    public void testNewEmployee() {
        Employee e = new Employee(1234, "James", "Architecture", 85000.0);

        assertEquals(1234, e.getEmployeeId());
        assertEquals("James", e.getName());
        assertEquals("Architecture", e.getDepartment());
        assertEquals(85000.0, e.getSalary());

        System.out.println("@Test testNewEmployee() " + System.identityHashCode(this));

    }
}
